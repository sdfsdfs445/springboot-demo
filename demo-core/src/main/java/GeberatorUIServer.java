import com.github.davidfantasy.mybatisplus.generatorui.GeneratorConfig;
import com.github.davidfantasy.mybatisplus.generatorui.MybatisPlusToolsApplication;
import com.github.davidfantasy.mybatisplus.generatorui.mbp.NameConverter;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

public class GeberatorUIServer {
    public static void main(String[] args) {
        URL location = GeberatorUIServer.class.getProtectionDomain().getCodeSource().getLocation();
        String path = location.getFile().replace("/target/classes/", "/src/main/resources/application.yml");
        System.out.println(path);
        try (InputStream inputStream = new FileInputStream(path)) {
            Yaml yaml = new Yaml();
            Map<String, Object> configMap = yaml.load(inputStream);

            String jdbcUrl = (String) configMap.get("jdbcUrl");
            String userName = (String) configMap.get("userName");
            String password = (String) configMap.get("password");
            String driverClassName = (String) configMap.get("driverClassName");
            String schemaName = (String) configMap.get("schemaName");
            String tablePrefix = (String) configMap.get("tablePrefix");
            String basePackage = (String) configMap.get("basePackage");
            int port = (int) configMap.get("port");

            // 输出读取到的属性值
            System.out.println("jdbcUrl: " + jdbcUrl);
            System.out.println("userName: " + userName);
            System.out.println("password: " + password);
            System.out.println("driverClassName: " + driverClassName);
            System.out.println("schemaName: " + schemaName);
            System.out.println("tablePrefix: " + tablePrefix);
            System.out.println("basePackage: " + basePackage);
            System.out.println("port: " + port);

        GeneratorConfig config = GeneratorConfig.builder().jdbcUrl(jdbcUrl)
                .userName(userName)
                .password(password)
                .driverClassName(driverClassName)
                //数据库schema，MSSQL,PGSQL,ORACLE,DB2类型的数据库需要指定
                .schemaName(schemaName)
                //数据库表前缀，生成entity名称时会去掉(v2.0.3新增)
                .tablePrefix(tablePrefix)
                //如果需要修改entity及其属性的命名规则，以及自定义各类生成文件的命名规则，可自定义一个NameConverter实例，覆盖相应的名称转换方法，详细可查看该接口的说明：                
                .nameConverter(new NameConverter() {
                    /**
                     * 自定义Service类文件的名称规则，entityName是NameConverter.entityNameConvert处理表名后的返回结果，如有特别的需求可以自定义实现
                     */
                    @Override
                    public String serviceNameConvert(String entityName) {
                        return entityName + "Service";
                    }

                    /**
                     * 自定义Controller类文件的名称规则
                     */
                    @Override
                    public String controllerNameConvert(String entityName) {
                        return entityName + "Controller";
                    }
                })
                //所有生成的java文件的父包名，后续也可单独在界面上设置
                .basePackage(basePackage)
                .port(8068)
                .build();
        MybatisPlusToolsApplication.run(config);
        System.out.println("访问 http://localhost:8068 进行生成");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}