# springboot-demo

#### 介绍
一些简单的学习模块


#### 安装教程

git clone https://gitee.com/sdfsdfs445/springboot-demo.git





#### demo1:集成了一个代码生成器

https://github.com/davidfantasy/mybatis-plus-generator-ui

使用：配置yml文件的配置项

![image-20240306010136885](README.assets/image-20240306010136885.png)

![image-20240306010154891](README.assets/image-20240306010154891.png)

运行server，访问 http://localhost:8068 进行生成

#### demo2:集成satoken和jwt

简单使用satoken，实现简单的RBAC权限管理，非常适合小白对权限管理的理解

#### demo3:简单crud

实现一些简单的对表crud及mybatisplus分页插件配置，文件上传，集成前端多功能编辑器wangedit

#### demo4:集成knif4j

#### 快速启动说明

后端：maven导入依赖,启动DemoApplication,数据库已经在开始自动创建



![image-20240306005802445](README.assets/image-20240306005802445.png)

```
前端：
npm install
npm run serve
```

![image-20240306005712182](README.assets/image-20240306005712182.png)

