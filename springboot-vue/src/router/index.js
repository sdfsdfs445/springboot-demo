import {createRouter, createWebHistory} from 'vue-router'
import Home from '../views/Home.vue'
import Layout from '../layout/Layout'

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: "/login",
    children: [
      {
        path: 'home',
        name: 'Home',
        component: () => import("@/views/Home"),
      },
      {
        path: 'book',
        name: 'Book',
        component: () => import('@/views/Book')
      },
      {
        path: 'about',
        name: 'about',
        component: () => import('../views/About.vue')
      },
      {
        path: 'echarts',
        name: 'echarts',
        component: () => import('../views/Echarts.vue')
      },
      {
        path: 'map',
        name: 'map',
        component: () => import('../views/Map.vue')
      },
      {
        path: 'news',
        name: 'news',
        component: () => import('../views/News.vue')
      },
      {
        path: 'im',
        name: 'Im',
        component: () => import("@/views/Im"),
      },
      {
        path: 'permissions',
        name: 'Permissions',
        component: () => import("@/views/Permissions"),
      },
      {
        path: 'roles',
        name: 'Roles',
        component: () => import("@/views/Roles"),
      },
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: ()=>import('../views/Login')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
