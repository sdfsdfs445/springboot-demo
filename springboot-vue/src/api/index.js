import request from '@/utils/request.js'

export function  getUserCountAddress(){
	return request.get("/user/count");
}
export function  getBookPage(params){
	return request.get("/book",{params});
}
export function  getBookById(id){
	return request.get("/book/"+id);
}
export function  addBook(data){
	return request.post("/book/create",data);
}
export function  deleteBook(id){
	return request.post("/book/delete/"+id);
}
export function  updateBook(data){
	return request.post("/book/update",data);
}
export function  deleteBooks(ids){
	return request.post("/book/deletes",ids);
}
export function  getNewsPage(params){
	return request.get("/news",{params});
}
export function  getNewsById(id){
	return request.get("/news/"+id);
}
export function  addNews(data){
	return request.post("/news/create",data);
}
export function  deleteNews(id){
	return request.post("/news/delete/"+id);
}
export function  updateNews(data){
	return request.post("/news/update",data);
}
export function  deleteNewss(ids){
	return request.post("/news/deletes",ids);
}

export function  getPermissionsPage(params){
	return request.get("/permissions",{params});
}
export function  getPermissionsById(id){
	return request.get("/permissions/"+id);
}
export function  addPermissions(data){
	return request.post("/permissions/create",data);
}
export function  deletePermissions(id){
	return request.post("/permissions/delete/"+id);
}
export function  updatePermissions(data){
	return request.post("/permissions/update",data);
}

export function  getRolesPage(params){
	return request.get("/roles",{params});
}
export function  getRolesById(id){
	return request.get("/roles/"+id);
}
export function  addRoles(data){
	return request.post("/roles/create",data);
}
export function  deleteRoles(id){
	return request.post("/roles/delete/"+id);
}
export function  updateRoles(data){
	return request.post("/roles/update",data);
}

export function getRolesByUserId(id){
	return request.get("/user-roles/roleslist/"+id);
}
export function  getRolesList(){
	return request.get("/roles/list");
}
export function  deleteUserRolesById(id){
	return request.post("/user-roles/delete/"+id)
}
export function  addUserRoles(data){
	return request.post("/user-roles/create",data)
}
export function  getPermissionsByRoleId(id){
	return request.get("/role-permissions/permissionlist/"+id);
}
export function getPermissionsList(){
	return request.get("/permissions/list");
}
export function  deleteRolesPermissionById(id){
	return request.post("/role-permissions/delete/"+id)
}
export function  addRolesPermission(data){
	return request.post("/role-permissions/create",data)
}