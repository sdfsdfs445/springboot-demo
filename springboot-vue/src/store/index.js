import { createStore } from 'vuex'

export default createStore({
  state: { // 存放数据 和data类似
    user:''
  },
  mutations: { // 用来修改state和getters里面的数据
    setUser(state,user){
      state.user=user;
    }
  },
  getters: { // 相当于计算属性
    getUser:(state) => state.user
  },
  actions: { // vuex中用于发起异步请求
    setUser({commit}, user) {
      this.state.user = user
    }
  },
  modules: {// 拆分模块
  }
})
