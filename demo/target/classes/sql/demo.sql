/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 06/03/2024 00:33:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '作者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '出版日期',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '封面地址',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES (1, '明朝那些事', 60.00, 'xxx', '2010-07-01 00:00:00', 'http://localhost:9999/files/a6719d7b86c14cce83ccb901c1ce14ef', 13);
INSERT INTO `book` VALUES (14, '555', 555.00, '55', '2023-06-05 00:00:00', 'http://localhost:9999/files/ce2e3d285e524dbc9c5944c2ca176763', NULL);
INSERT INTO `book` VALUES (15, NULL, 111.00, '111', '2023-05-30 00:00:00', NULL, NULL);
INSERT INTO `book` VALUES (16, '222', 333.00, '4444', '2024-03-03 00:00:00', NULL, NULL);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '作者',
  `time` datetime NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (4, '4444', '<p>444444</p><p><br/></p><p>44444</p><p><br/></p><p><br/></p><p>555555<img src=\"https://img1.baidu.com/it/u=4110196045,3829597861&amp;fm=26&amp;fmt=auto&amp;gp=0.jpg\" contenteditable=\"false\" style=\"font-size: 14px; max-width: 100%;\"/></p>', '管理员', '2021-07-17 18:55:27');
INSERT INTO `news` VALUES (5, '77777', '<p>7777</p><p><br/></p><p><img src=\"http://localhost:9090/files/c536f6f2f0e94983951240d73d740601\" style=\"max-width:100%;\" contenteditable=\"false\" width=\"388\" height=\"388\"/><br/></p><p>8888</p><p><br/></p><p><img src=\"http://localhost:9090/files/b344314319f047cf9192ce64ca454674\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', '管理员', '2021-07-17 19:14:14');
INSERT INTO `news` VALUES (6, '带你从0搭建一个Springboot+vue前后端分离项目，真的很简单！', '<p><iframe src=\"//player.bilibili.com/player.html?aid=803885504&amp;bvid=BV14y4y1M7Nc&amp;cid=361690131&amp;page=1\" scrolling=\"no\" border=\"0\" frameborder=\"no\" framespacing=\"0\" allowfullscreen=\"true\" width=\"600\" height=\"400\"> </iframe></p>', '管理员', '2021-09-05 22:28:25');
INSERT INTO `news` VALUES (7, '我擦！妹子给我整害羞了⁄(⁄ ⁄ ⁄ω⁄ ⁄ ⁄)⁄……', '<p><iframe src=\"//player.bilibili.com/player.html?aid=717601909&amp;bvid=BV1sQ4y1174g&amp;cid=396410411&amp;page=1\" scrolling=\"no\" border=\"0\" frameborder=\"no\" framespacing=\"0\" allowfullscreen=\"true\" width=\"600\" height=\"400\"> </iframe></p>', '管理员', '2021-09-05 22:31:40');
INSERT INTO `news` VALUES (34, '视频测试', '<p><br></p><div data-w-e-type=\"video\" data-w-e-is-void>\n<video poster=\"\" controls=\"true\" width=\"auto\" height=\"auto\"><source src=\"http://localhost:9999/files/9f8a4e1057434492bfbd01b92f12ab4e\" type=\"video/mp4\"/></video>\n</div><p><br></p><div data-w-e-type=\"video\" data-w-e-is-void>\n<iframe src=\"//player.bilibili.com/player.html?aid=717601909&amp;bvid=BV1sQ4y1174g&amp;cid=396410411&amp;page=1\" scrolling=\"no\" border=\"0\" frameborder=\"no\" framespacing=\"0\" allowfullscreen=\"true\" width=\"600\" height=\"400\"> </iframe>\n</div><p><br></p>', 'liyedong', '2023-06-08 20:56:31');
INSERT INTO `news` VALUES (35, '上传图片测试和base64图片测试', '<p><img src=\"http://localhost:9999/files/7255a1d978754dadaea57ba2c7e35ab0\" alt=\"\" data-href=\"\" style=\"\"/><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAYAAAA8AXHiAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABA5SURBVHhe7Z0PjBxVHcffmytU/pX/UNrbuxaq3F7RgEoiSCsgKEiIYIwmRv7sbishIYj/gkBAMILRCIqSRmhvD2KD/LHSWGmvUIRABUqhtdjb3aul5f60QCkUpIVCe/P8fnfmarnu3v6ZmTezO+/TfO+9mevtzp/v/N6/mXlSeCCZHexEMgM6FWqHpkDHQW9DG6FBaI0QqufDXbtyG644YRjLhhhQs7GS814dZ0s5zZLyK/jzC7GK5poAtUDjXNFAu930PWgD1INv68mnEiuRDwyY/SAk3BaDb0gbwcHOpxPvuysqUpOxktmhA4Wwv4o/S2NxGsTodDA01omkuXZCr0P9+NustMSi3OWt/+Uv/cSNoN+EPg0dwHUGX2CQGJJCzhs3btyaly+dqJzV5anKWB3ZTZZUw4cLKS/A4negM6Hx/F2NcANfhBZJKR7IpRKMZL6QzG46BKb/IbKXQq3Q/lxv8AUGh3egBVLKG3Kp1q3FtWNguenYKJum+jpys6AzoHpMRVhMsj6WUUpcgggzqbjWBxS3UYgvQcdDxlT+whLpSOhCpdSxxTUVqGisZNcAqlPidGR/CrGizjqMF7iRPPmMLhfBXD4VWYr1vKOcvCEgjsFx5gVckTGNhZPOovIw6Erok1znIzTCNdC0ju5NnlqnDorRcD8nbwgIBAXJ41yRShFrAorAnyE931n0HZr1ZuiI4pKhaahgLPll/LjMyQfG+VLZmendG32IWoaoUNZYn7v7RYkaMY2F1lagfAI61xYtVVUKDY1BWWN9sP/RU4QUpyFbqbj0CiPVdKHkKc6ioRkoaZrp3QNSKWtkmEYHbMqe0dk1UFXF0BB9ShpLKcnWFaPVocUVwcN+p3o7XQ0RpLSxnBM8FdI55pZUsrqmrCH6lDQWKj08wbo7GydYynPnqyEilItYbKlxvE0nLcNCsUfe0ASUNBZgERjC8Ig8xs3UQ8URd4M+yhkrFKRQu9xsHUjemsO7JwwRoJyxeJvEdierlYq3Y5RH8m/XQsZcEaCcsXhyfL8RrwK7lRRb3HztSPkOfixE7gXoNehNj+L9Rx4iaFl4bLdBpb7TT/FC4z6EQsnxuWR2kLdGPAZ9vrhCD2+hkjStkE7UfTA6ugbHSyl4u/RMVLk83o4jD8SPDugzkF/dICyuGVXXYfuqvs23PiSDBveBIxrsOvLrHrWz8unEU26+LCWN1ZEdOEAKuQDZ8yBdg8OrEHVm5FOtng94sqv/MJjU44HEiZHiLByHO7Aw0VnnmfVC2ldL0fJvW9m7hFKBNjiktHhBnA39AvJrFKV+Y50491XLarGuwK95UNn1EDQscn6DDb7eWYwOiN7XIbnNWfIETXSzJcbd2ps+jnVYLXRmB8fji3+J7NWQHx3eVRmrZB2rb/YUGxvzKLL113lqYxAmXurmo4UUy/HTj4bMh0rYa3SaiuTSCXyvegbZIOqLZRmru4FP1dBcOiigUMi5+WihxA78/MBZ8IgSH7k5vUjpz/bXQFljFdJtPAgPQKhoBgofbl2obMU0itiQX1Em0DpVWRCy3Jw2xopYQK3GD0atoMIoW0k9qMM+2Tc7obWIMATLmMbKp9v4FPP9EJvIQbg+D/0R7S8+jm9oIipELJorwQdM74PegvwyFz9nuxTqL6hbrcin2ky0ChDeY+5mtVHRWEQKMQ/JXRCLLj+gkR5SQs4pZBLhVGhjha6uyP9TlbHQZN2ByHUL4gw7TFc4a+umF7oKn5eBQhtyiBfaA1Z1xtqDkith/quwmQ9i6Q2ILaZq4PgYx64Wo6L+E+znQ8W1Bk3IaBaFI+RntbI/hP1Nv4L4iDzrXoxANBmHYvbeAZqO64awY4sRjG9C/ufQ8zDou0gNTUzdhW9Htv9gKawTkeUrgziWxkfm+Tg+H8BgvYkdi+yxRqSSayxl/2vCIXLrc99K6I/LHkhmB09GsgTyOl64Uyn74kKmvcdd1kZn99C5KCn+hqwfw3P1D+lUQyHdvl21yFWIsgsgRq57hJRzYFUIKSrmCGD3IJ0vpXi6N9P2ZqOZqmkI4ajrby40GM0QsZJdg+fgTC9CNvoRy9A4KBQZujHGigXVNt79wxgrDqiIdzcYGhO3JNRqLmMsQyAYY8WBELobjLHigNRvLWOsOGC6GwyBoP92LGOsOBDZG/0MDY4pCg1BEPAD1yUxxooBKuo3+hkalBDuYTHGigOmKDQEgQzhNBtjxQLT3WAIAv0loTFWLNDfKDTGigXSdDcYmgTZ2T20n1KKT6AcjlT7XDZSyl2oA2xVtnijMDuh/+bsCjTFc4XZwZkIWfxeP+bfru4dpDhwCaQXQ5+FsQ4urtUIjPUejPUsso/kMwkP73kPBmOsfajaWNci/RF0dHFNOPCd8j+2xLis7nd0VqJJjDUDxuI7XrUZi3Ws70Nhmorw8fxZSuz2613khr2wQxjTobFCmIypJIfiqtI5P2KM0N4oLBorKpNPtoQxphUHZEjPFYYw9l0SK4wb0uJBOBHL0OzEu+edO2+KwkCI9/1YEkQygLIO6tepCWcHlRVKHSsqyDDuza4EtmkSkiOcJU9IIS0/PqcOzNtmIkOye9BKZocmIox+G4v7OWs9MR5h76pk10D79OxrWrtV4Gg3pw/2vEclTPRDJ+XTibpn2uro6u/EQZyKUtW7EaQ4FFW+05HjcJdfHcjcN74x+iWIk2DVGErkFjGs+vKzE5zMoWo6s4On4yQ/jiwnxvRK1UM6UTHWAE7kSflMgtOs1Az244tIUtDxkB99c3ytYht0bHHJPzitLk3FYaxayygem7Uw/d35VGKDs6oyxljejPV7JCy2+OZmP2I/P4NFVhDlCA1F1Xrs+f+3YJNuzKdb73VWVSYMY7GOpb9mVwaP/aMXQcdAHG9kUehVfrYGR8Pjzs8v9b1jifvWCs2AqiaMyMEd1D5JYhm8duNNdtMYUNvtTWE9sNrnZMNGITp42v84tXBrm4YvlJ53q+W7SPkOcD/mPfYAS504eaNueqVo+Z2br44QKjs8k+vhZ84ef0UNuhLB5Qakr0B+4TVixQHOlsZZQNiyrBrUXfUXhfnLJ3Fa3vUQWw2MXNWI87LcDyPciNSvojSEgN1gKLEGPxbl0pMiP3Gop1ZPR1f/BCktTpB5ibPGE5uxNR35VN3dDc3uy20w1nVSDWdzs6bUNEd3smvwCzi2TyCrtbuhbgqZdnbyMdoZgoUR6iWYY1mtpioSVKfJGHiuLSulaC4/CKEm0DBsgx5EXWHIWayNMCqvphkWfRT+vYxT9fdcJvGhu642Yv4aI9MqLM1WNGtuz6cnc3yxPkK4zy1axjK+Gs1OHJMF0Ep3uT7iPoGA9DZYGJkxTx8ZgHibDetYdWPHvSj0uPuc8LyZ4ETti3CxvZTPtO12VtVNvCOWR/gIeV19YBFlBbQwl271odUd84jlsSycCz0KsT6yygdxRIFDKH7DYo2fvRoq9b3c/n8opeYpZdNcDYnnrrOOroFr4Iffuote4PjXtHw6UfcVmsz2TxVKJtE+997DbFkJlM3nITcT8mOSbsLuAvaAL4Np2CdVIpTInZaU62xl9xcy7fV1L4wi2TV4Ks40e8v13UHqpnXjo7H4CqMTvBjLTzq6N1qWGNeJyPEHHKYz3dVeWQkrpfKZRK+7rIVkFsYSeo0VrTqWZ5v7RyE11c6lEmtxiDgWWvswyr7wM+7UbSqXWFfeZTQ7DBSb/H5EUbT61btuXisy9j3v0bxxhpHGa3OfYOdUOLe7hPCylYh1N0SoLPw4fjk+rCtH+/caYxkCIVpFYWgXdHMTxlGNmLFMxAoCPv+km4gVhSZiBUJIr4qMCiZcBYb+CzZaxjLWCgTEq5hHLFMSBkMIxzVidSxDIJjKuykLA8E5rFrjVqSKQuy5cVYAyLjPVxjGYGkssPWP7ker8h7CYGkssPQf12gZyzQLAyHu3Q2GgAjjco1YxDIEgRkrNASCiViGYIh9HctYKxhiXhSascKgCOG4mqIwBujvdzeV93gQwpCGiVhxIIT+BhOxDIFgjBUDQplAwE2jgCkKm4iIGSuMwYfmx7QKTdAKhGEr5rPYu6nBb2J/o58hGGL/thljrUCIe6uQlcwD3KwhunBu6opEqihUSkxx8wY/8TNeSXGcmxuTqBWFM5LZwfEdXUMtHXMHrX00b8AUlvXgZ3+DEqfhHHEG/jGJlrGE+J6r6VBbCR3LtxkjNdSEr1Wsb0AzOu7t399ZLI3nCODj67gJ28WcXYIv7i/1pmK+w3OtFPL2XLr1OWdVsODqPBnJEmhicUX97FTKvriQae9xl7XR2TWYVLI4McFBzhpP0KWcBGEt9BpX7MUH0GJLiWVRu/q5PYdAk6BSEYt1sLOUUNfihB+GvKEKfO5t4Kfx2J8CnTNKF0A32lL8OorFCreJLQ+G2tEaDx0BcbaIVshQHX53N4wEgCNH6WiI1ZiLomisasAVI03XRJX47aoK0FNHNaqxtiC+b3fzhgq4D+no9NfORjQWD9A/hbLfdhYNlVD6h3Q2N6Kx2PJYCne95SwaKqL/ucInG9FYqy1lLSikE35MQ6KbHW7azORQ/72r0Yw1jI1mP5CnOZJDgBFjM7YdB10/Wt+7ptRy6JVGM9YQtnxpb2Zyo00svh66Bwpl9i+Ns6pxlrSFMNaORjLWR9ByGOs/zmJDwBlSOR3vHISNhwveJw2vFx0hi6Miz6KZsDI/q81uJGOxfvK4Ug1TT2Ejg8Mof8JZfdi27VeLa8NAT6twO+y7RCpZbK03krH60bh5Ble9H7OdBgmjA4u8FcjeherNfWhobOrLtIVWfGuqYq1XUj3ROytR3M9GMRY39qndw1a/sxhZeAp3Qhwgv9VWuxfmMonwGxrBP2HPL3gaDt7gLDaOsV7Hts9fP7s1nBlKq4ejAfOFtC7PpxPL+jLH+zILvVdk8E/pvIMv+HMh1c7iv0ijGGuZUFZ4dZTKsHhmy+8HQorrkW7hyqiggj3LbJA8IoXcE61IIxgLGy4fRxpOU706WEm/CVqMA7wtn5oceNlTCwE//cXz8qib7qERjLUGWp7PtIbVVOdpKXdq2AJaCN2JSNVjSfl6LhXF4jrQStYL0PP59MfPjx/GCrL5z76rHqnUJmcxFFhv4F2to3kT4pU6F3osn0ps6021RipS7UEWi6sgtg11SLlECvGGu7wHP4z1ChTULSwbhZRL0LIKr4tBSQ52s5XH1h4jF1PWJxip5gy3iKWoqPNW6siihOKFwQvBTxiZCzDVE7l0Yp8o7dlYUlqrkfRCjC4jVwXTEY0UJSPiRuwtXk17iybiZ9Gsj+Ej1iENDRTBLO6yUAHildmHvZqvhH0bDuqqdZfte1CjRl+qjRfHMxCjb6lzNPo8lTs/PDcUW7s8Ln/Fn25Gug+eu2Q7skP7S6G+huwtUCfEzxy9kdxwpnuvH/ldqXUsXldJKW7LpRIfa22ERbJ7aCaqKnymbptltyzvnTXpfec3jQG2/1NSiTsQvc7GIm/xHn3s914eyZdL2Te30j0/ox+oAEL8D8AnT9deckIuAAAAAElFTkSuQmCC\" alt=\"info.png\" data-href=\"\" style=\"\"/></p>', 'liyedong', '2023-06-08 20:57:03');
INSERT INTO `news` VALUES (36, NULL, '<p>模拟 Ajax 异步设置内容<img src=\"http://localhost:9999/files/25cc80a786ee4d5a95db55fdd14d6b9b\" alt=\"\" data-href=\"\" style=\"\"/></p>', 'liyedong', '2023-06-09 18:27:02');
INSERT INTO `news` VALUES (37, NULL, '<p>模拟 Ajax 异步设置内容</p><div data-w-e-type=\"video\" data-w-e-is-void>\n<video poster=\"\" controls=\"true\" width=\"auto\" height=\"auto\"><source src=\"http://localhost:9999/files/d6fd6fc66420445ebc536fec6b239ac5\" type=\"video/mp4\"/></video>\n</div><p><br></p>', 'liyedong', '2023-06-14 19:15:00');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `permission_id` int NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`permission_id`) USING BTREE,
  UNIQUE INDEX `permission_name`(`permission_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'add');
INSERT INTO `permissions` VALUES (5, 'book');
INSERT INTO `permissions` VALUES (2, 'delete');
INSERT INTO `permissions` VALUES (6, 'news');
INSERT INTO `permissions` VALUES (4, 'select');
INSERT INTO `permissions` VALUES (3, 'update');

-- ----------------------------
-- Table structure for role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions`  (
  `role_permission_id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NULL DEFAULT NULL,
  `permission_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`role_permission_id`) USING BTREE,
  INDEX `role_id`(`role_id` ASC) USING BTREE,
  INDEX `permission_id`(`permission_id` ASC) USING BTREE,
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES (1, 1, 1);
INSERT INTO `role_permissions` VALUES (2, 1, 2);
INSERT INTO `role_permissions` VALUES (4, 1, 4);
INSERT INTO `role_permissions` VALUES (5, 2, 1);
INSERT INTO `role_permissions` VALUES (6, 2, 2);
INSERT INTO `role_permissions` VALUES (7, 2, 3);
INSERT INTO `role_permissions` VALUES (8, 2, 4);
INSERT INTO `role_permissions` VALUES (9, 1, 5);
INSERT INTO `role_permissions` VALUES (10, 2, 6);
INSERT INTO `role_permissions` VALUES (11, 1, 6);
INSERT INTO `role_permissions` VALUES (12, 5, 3);
INSERT INTO `role_permissions` VALUES (13, 4, 4);
INSERT INTO `role_permissions` VALUES (14, 4, 6);
INSERT INTO `role_permissions` VALUES (15, 4, 2);
INSERT INTO `role_permissions` VALUES (16, 4, 5);
INSERT INTO `role_permissions` VALUES (17, 4, 1);
INSERT INTO `role_permissions` VALUES (18, 4, 3);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `role_name`(`role_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (4, '333');
INSERT INTO `roles` VALUES (1, 'admin');
INSERT INTO `roles` VALUES (5, 'superadmin');
INSERT INTO `roles` VALUES (2, 'user');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '昵称',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `account` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '账户余额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '$2a$10$.cxJ637DYA1PRSzfPmBO0efS1W2eTQefS51b83Nr5xgRNAW0ffgjO', '管理员', 32, '男', '火星', 'http://localhost:9090/files/888f2d39d0724816a738a716d56ad58a', 100000.00);
INSERT INTO `user` VALUES (13, 'zhang', '$2a$10$h6gfZRMvGFjjQH6HrQf.Q.fvu3jjMc0cv/sGpSanobht5iwCFbucS', '张三', 20, '男', '木星', 'http://localhost:9090/files/d227473b758e4915a3e7c866d2d494cf', 200.00);
INSERT INTO `user` VALUES (14, 'wang', '$2a$10$h6gfZRMvGFjjQH6HrQf.Q.fvu3jjMc0cv/sGpSanobht5iwCFbucS', '王梦晨', 24, '女', '地球', NULL, 300.00);
INSERT INTO `user` VALUES (15, 'li', '$2a$10$h6gfZRMvGFjjQH6HrQf.Q.fvu3jjMc0cv/sGpSanobht5iwCFbucS', '李雪', 22, '女', '银河系', NULL, 500.00);
INSERT INTO `user` VALUES (16, 'qian', '$2a$10$h6gfZRMvGFjjQH6HrQf.Q.fvu3jjMc0cv/sGpSanobht5iwCFbucS', '钱江', 22, '男', '地球', NULL, 700.00);
INSERT INTO `user` VALUES (17, 'liyedong', 'liyedong', 'liyedong', 22, '男', '球', NULL, 0.00);
INSERT INTO `user` VALUES (18, '444', NULL, '333', NULL, NULL, '333', NULL, 0.00);
INSERT INTO `user` VALUES (23, '2222', NULL, '222', NULL, NULL, '222', NULL, 0.00);
INSERT INTO `user` VALUES (24, '3333', NULL, '333', NULL, NULL, '333', NULL, 0.00);
INSERT INTO `user` VALUES (25, '444', NULL, '444', NULL, NULL, '3333', NULL, 0.00);
INSERT INTO `user` VALUES (26, '444', NULL, '444', NULL, NULL, '444', NULL, 0.00);
INSERT INTO `user` VALUES (27, '44', NULL, '444', NULL, NULL, '44', NULL, 0.00);
INSERT INTO `user` VALUES (28, '11', NULL, '111', NULL, NULL, '111', NULL, 0.00);
INSERT INTO `user` VALUES (29, '255', NULL, '555', NULL, NULL, '55', NULL, 0.00);
INSERT INTO `user` VALUES (30, 'yang', 'yang', 'yang', NULL, NULL, 'yang', NULL, 0.00);
INSERT INTO `user` VALUES (31, 'yedong', '$2a$10$yoVO.tobDTUkbJy44Xqiu.IwSlxgMBiYb7e/4bbNBHW6PZiCo9s8y', '用户1667559532831784961', NULL, NULL, NULL, NULL, 0.00);

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles`  (
  `user_role_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `role_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`user_role_id`) USING BTREE,
  INDEX `user_id`(`user_id` ASC) USING BTREE,
  INDEX `role_id`(`role_id` ASC) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES (1, 17, 1);
INSERT INTO `user_roles` VALUES (2, 17, 2);
INSERT INTO `user_roles` VALUES (3, 30, 2);
INSERT INTO `user_roles` VALUES (4, 17, 4);
INSERT INTO `user_roles` VALUES (5, 1, 4);
INSERT INTO `user_roles` VALUES (6, 1, 1);
INSERT INTO `user_roles` VALUES (7, 1, 2);
INSERT INTO `user_roles` VALUES (8, 13, 2);
INSERT INTO `user_roles` VALUES (9, 14, 2);
INSERT INTO `user_roles` VALUES (10, 15, 2);
INSERT INTO `user_roles` VALUES (11, 16, 2);
INSERT INTO `user_roles` VALUES (12, 23, 2);
INSERT INTO `user_roles` VALUES (13, 24, 2);
INSERT INTO `user_roles` VALUES (14, 25, 2);
INSERT INTO `user_roles` VALUES (15, 30, 1);

SET FOREIGN_KEY_CHECKS = 1;
