package com.liyedong.demo.common;

import cn.dev33.satoken.stp.StpUtil;
import com.liyedong.demo.entity.User;
import com.liyedong.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
public class BaseController {

    @Resource
    private UserService userService;

    @Resource
    private HttpServletRequest request;

    /**
     * 根据token获取用户信息
     * @return user
     */
    public User getUser() {
        log.info("roles:{}",StpUtil.getRoleList().toString());
        log.info("permissions:{}",StpUtil.getPermissionList().toString());
        return userService.getById(StpUtil.getLoginId().toString());
    }
}
