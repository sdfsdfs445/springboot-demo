package com.liyedong.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liyedong.demo.entity.News;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-07
 */
public interface NewsMapper extends BaseMapper<News> {

}
