package com.liyedong.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liyedong.demo.entity.Permissions;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
public interface PermissionsMapper extends BaseMapper<Permissions> {

}
