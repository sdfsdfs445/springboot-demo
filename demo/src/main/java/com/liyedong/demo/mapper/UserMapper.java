package com.liyedong.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liyedong.demo.entity.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-01
 */
public interface UserMapper extends BaseMapper<User> {
}
