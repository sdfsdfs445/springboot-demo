package com.liyedong.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liyedong.demo.entity.Book;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-05
 */
public interface BookMapper extends BaseMapper<Book> {

}
