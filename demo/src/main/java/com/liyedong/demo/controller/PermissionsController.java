package com.liyedong.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.Permissions;
import com.liyedong.demo.service.PermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
@RestController
@RequestMapping("/permissions")
public class PermissionsController {


    @Autowired
    private PermissionsService permissionsService;

    @GetMapping
    public Result<Page<Permissions>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        LambdaQueryWrapper<Permissions> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByAsc(Permissions::getPermissionId);
        Page<Permissions> aPage = permissionsService.page(new Page<>(current, pageSize),lambdaQueryWrapper);
        return Result.success(aPage);
    }

    @GetMapping(value = "/{id}")
    public Result<Permissions> getById(@PathVariable("id") String id) {
        return Result.success(permissionsService.getById(id));
    }

    @PostMapping(value = "/create")
    public Result<Object> create(@RequestBody Permissions params) {
        permissionsService.save(params);
        return Result.success("created successfully");
    }

    @PostMapping(value = "/delete/{id}")
    public Result<Object> delete(@PathVariable("id") String id) {
        permissionsService.removeById(id);
        return Result.success("deleted successfully");
    }

    @PostMapping(value = "/update")
    public Result<Object> update(@RequestBody Permissions params) {
        permissionsService.updateById(params);
        return Result.success("updated successfully");
    }
    @GetMapping("/list")
    public Result<Object> list() {
        List<Permissions> roles = permissionsService.list();
        return Result.success(roles);
    }
}
