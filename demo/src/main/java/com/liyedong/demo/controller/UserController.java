package com.liyedong.demo.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.BaseController;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.User;
import com.liyedong.demo.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-01
 */
@SaCheckLogin
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {


    @Resource
    private UserService userService;
    @PostMapping
    public Result<?> save(@RequestBody User user){
        userService.save(user);
        return Result.success();
    }
    @GetMapping("/{id}")
    public Result<?> getOne(@PathVariable Integer id){
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getId,id);
        User user = userService.getOne(queryWrapper);
        if (user!=null){
            return Result.success(user);
        }else {
            return Result.error("1","找不到该用户");
        }
    }
    @GetMapping("/loginUser")
    public Result<?> getLoginUser(){
        User user = getUser();
        if (user!=null){
            return Result.success(user);
        }else {
            return Result.error("1","找不到该用户");
        }
    }
    @PutMapping
    public Result<?> update(@RequestBody User user){
        userService.updateById(user);
        return Result.success();
    }
    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable Integer id){
        userService.removeById(id);
        return Result.success();
    }
    @GetMapping("/page")
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search){
        Page<User> page=new Page<>(pageNum,pageSize);
        LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
        wrapper.orderByAsc(User::getId);
        if (search!=null){
            wrapper.like(User::getNickName,search);
        }
        userService.page(page, wrapper);
        return Result.success(page);
    }
    @SaIgnore
    @PostMapping("/login")
    public Result<?> login(@RequestBody User user){
        LambdaQueryWrapper<User> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(User::getUsername,user.getUsername());
        wrapper.eq(User::getPassword,user.getPassword());
        User one = userService.getOne(wrapper);
        if (one!=null){
            StpUtil.login(one.getId());
            return Result.success(StpUtil.getTokenInfo().getTokenValue());
        }
        return Result.error("1","账号密码错误");
    }
    @GetMapping("/count")
    public Result<?> count() {
        QueryWrapper<User> queryWrapper=new QueryWrapper<>();
        queryWrapper.select("count(id) as count,address").groupBy("address");
        List<Map<String, Object>> maps = userService.listMaps(queryWrapper);
        return Result.success(maps);
    }
}
