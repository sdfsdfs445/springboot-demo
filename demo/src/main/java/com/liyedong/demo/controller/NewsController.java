package com.liyedong.demo.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.BaseController;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.News;
import com.liyedong.demo.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-05
 */
@SaCheckPermission("news")
@SaCheckLogin
@RestController
@RequestMapping("/news")
public class NewsController extends BaseController {


    @Autowired
    private NewsService newsService;

    @GetMapping
    public ResponseEntity<?> list(@RequestParam(required = false) Integer current,
                                           @RequestParam(required = false) Integer pageSize,
                                           @RequestParam(required = false) String search) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        LambdaQueryWrapper<News> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(News::getId);
        if (StrUtil.isNotEmpty(search)){
            queryWrapper.like(News::getTitle,search);
        }

        Page<News> aPage = newsService.page(new Page<>(current, pageSize),queryWrapper);
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(newsService.getById(id), HttpStatus.OK);
    }

    @PostMapping("/create")
    public Result<?> create(@RequestBody News params) {
        params.setTime(new Date());
        newsService.save(params);
        return Result.success();
    }

    @PostMapping(value = "/delete/{id}")
    public Result<?> delete(@PathVariable("id") String id) {
        
        newsService.removeById(id);
        return Result.success();
    }

    @PostMapping(value = "/update")
    public Result<?> update(@RequestBody News params) {
        newsService.updateById(params);
        return Result.success();
    }
    @PostMapping(value = "/deletes")
    public Result<?> delete(@RequestBody List<Integer> ids) {
        newsService.removeByIds(ids);
        return Result.success();
    }
}
