package com.liyedong.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.Roles;
import com.liyedong.demo.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
@RestController
@RequestMapping("/roles")
public class RolesController {


    @Autowired
    private RolesService rolesService;

    @GetMapping(value = "")
    public Result<Page<Roles>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        LambdaQueryWrapper<Roles> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(Roles::getRoleId);
        Page<Roles> aPage = rolesService.page(new Page<>(current, pageSize),queryWrapper);
        return Result.success(aPage);
    }

    @GetMapping(value = "/{id}")
    public Result<Roles> getById(@PathVariable("id") String id) {
        return Result.success(rolesService.getById(id));
    }

    @PostMapping(value = "/create")
    public Result<Object> create(@RequestBody Roles params) {
        rolesService.save(params);
        return Result.success("created successfully");
    }

    @PostMapping(value = "/delete/{id}")
    public Result<Object> delete(@PathVariable("id") String id) {
        rolesService.removeById(id);
        return Result.success("deleted successfully");
    }

    @PostMapping(value = "/update")
    public Result<Object> update(@RequestBody Roles params) {
        rolesService.updateById(params);
        return Result.success("updated successfully");
    }
    @GetMapping("/list")
    public Result<Object> list() {
        List<Roles> roles = rolesService.list();
        return Result.success(roles);
    }
}
