package com.liyedong.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.Permissions;
import com.liyedong.demo.entity.RolePermissions;
import com.liyedong.demo.service.PermissionsService;
import com.liyedong.demo.service.RolePermissionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-04
 */
@RestController
@RequestMapping("/role-permissions")
public class RolePermissionsController {


    @Autowired
    private RolePermissionsService rolePermissionsService;
    @Autowired
    private PermissionsService permissionsService;
    @GetMapping(value = "")
    public Result<Page<RolePermissions>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<RolePermissions> aPage = rolePermissionsService.page(new Page<>(current, pageSize));
        return Result.success(aPage);
    }

    @GetMapping(value = "/{id}")
    public Result<RolePermissions> getById(@PathVariable("id") String id) {
        return Result.success(rolePermissionsService.getById(id));
    }

    @PostMapping(value = "/create")
    public Result<Object> create(@RequestBody RolePermissions params) {
        rolePermissionsService.save(params);
        return Result.success("created successfully");
    }

    @PostMapping(value = "/delete/{id}")
    public Result<Object> delete(@PathVariable("id") String id) {
        rolePermissionsService.removeById(id);
        return Result.success("deleted successfully");
    }

    @PostMapping(value = "/update")
    public Result<Object> update(@RequestBody RolePermissions params) {
        rolePermissionsService.updateById(params);
        return Result.success("updated successfully");
    }
    @GetMapping(value = "/list/{rolesId}")
    public Result<List<Permissions>> listByUserId(@PathVariable String rolesId) {
        LambdaQueryWrapper<RolePermissions> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(RolePermissions::getRoleId,rolesId);
        List<RolePermissions> rolePermissions = rolePermissionsService.list(queryWrapper);
        LambdaQueryWrapper<Permissions> rolesQueryWrapper=new LambdaQueryWrapper<>();
        for (int i=0;i<rolePermissions.size();i++){
            rolesQueryWrapper.eq(Permissions::getPermissionId,rolePermissions.get(i).getRoleId());
            if (i!=rolePermissions.size()-1){
                rolesQueryWrapper.or();
            }
        }
        List<Permissions> permissions = permissionsService.list(rolesQueryWrapper);
        return Result.success(permissions);
    }
    @GetMapping(value = "/permissionlist/{roleId}")
    public Result<List<RolePermissions>> listByRolesId(@PathVariable String roleId) {
        LambdaQueryWrapper<RolePermissions> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(RolePermissions::getRoleId,roleId);
        List<RolePermissions> rolePermissions = rolePermissionsService.list(queryWrapper);
        if (rolePermissions.size()==0){
            return Result.success(null);
        }
        return Result.success(rolePermissions);
    }
}

