package com.liyedong.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.BaseController;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.UserRoles;
import com.liyedong.demo.service.RolesService;
import com.liyedong.demo.service.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-04
 */
@RestController
@RequestMapping("/user-roles")
public class UserRolesController extends BaseController {


    @Autowired
    private UserRolesService userRolesService;
    @Autowired
    private RolesService rolesService;

    @GetMapping(value = "")
    public Result<Page<UserRoles>> list(@RequestParam(required = false) Integer current, @RequestParam(required = false) Integer pageSize) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        Page<UserRoles> aPage = userRolesService.page(new Page<>(current, pageSize));
        return Result.success(aPage);
    }
    @GetMapping(value = "/roleslist/{userId}")
    public Result<List<UserRoles>> listByUserId(@PathVariable String userId) {
        LambdaQueryWrapper<UserRoles> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserRoles::getUserId,userId);
        List<UserRoles> userRoles = userRolesService.list(queryWrapper);
        if (userRoles.size()==0){
            return Result.success(null);
        }
        return Result.success(userRoles);
    }
    @GetMapping(value = "/{id}")
    public Result<UserRoles> getById(@PathVariable("id") String id) {
        return Result.success(userRolesService.getById(id));
    }

    @PostMapping(value = "/create")
    public Result<Object> create(@RequestBody UserRoles params) {
        userRolesService.save(params);
        return Result.success("created successfully");
    }

    @PostMapping(value = "/delete/{id}")
    public Result<Object> delete(@PathVariable("id") String id) {
        userRolesService.removeById(id);
        return Result.success("deleted successfully");
    }

    @PostMapping(value = "/update")
    public Result<Object> update(@RequestBody UserRoles params) {
        userRolesService.updateById(params);
        return Result.success("updated successfully");
    }
}
