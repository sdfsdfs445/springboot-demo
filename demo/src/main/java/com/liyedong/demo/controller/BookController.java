package com.liyedong.demo.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.liyedong.demo.common.BaseController;
import com.liyedong.demo.common.Result;
import com.liyedong.demo.entity.Book;
import com.liyedong.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-05
 */
@SaCheckPermission("book")
@SaCheckLogin
@RestController
@RequestMapping("/book")
public class BookController extends BaseController {


    @Autowired
    private BookService bookService;

    @GetMapping
    public ResponseEntity<?> list(@RequestParam(required = false) Integer current,
                                           @RequestParam(required = false) Integer pageSize,
                                           @RequestParam(required = false) String search) {
        if (current == null) {
            current = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        LambdaQueryWrapper<Book> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(Book::getId);
        if (StrUtil.isNotEmpty(search)){
            queryWrapper.like(Book::getName,search);
        }
        Page<Book> aPage = bookService.page(new Page<>(current, pageSize),queryWrapper);
        return new ResponseEntity<>(aPage, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String id) {
        return new ResponseEntity<>(bookService.getById(id), HttpStatus.OK);
    }

    @PostMapping("/create")
    public Result<?> create(@RequestBody Book params) {
        bookService.save(params);
        return Result.success();
    }

    @PostMapping(value = "/delete/{id}")
    public Result<?> delete(@PathVariable("id") String id) {
        bookService.removeById(id);
        return Result.success();
    }

    @PostMapping(value = "/update")
    public Result<?> update(@RequestBody Book params) {
        bookService.updateById(params);
        return Result.success();
    }
    @PostMapping(value = "/deletes")
    public Result<?> delete(@RequestBody List<Integer> ids) {
        bookService.removeByIds(ids);
        return Result.success();
    }
}
