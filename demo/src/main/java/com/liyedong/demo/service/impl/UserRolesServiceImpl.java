package com.liyedong.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liyedong.demo.entity.UserRoles;
import com.liyedong.demo.mapper.UserRolesMapper;
import com.liyedong.demo.service.UserRolesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
@Service
public class UserRolesServiceImpl extends ServiceImpl<UserRolesMapper, UserRoles> implements UserRolesService {

}
