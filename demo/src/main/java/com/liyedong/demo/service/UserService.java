package com.liyedong.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liyedong.demo.entity.User;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-01
 */
public interface UserService extends IService<User> {

}
