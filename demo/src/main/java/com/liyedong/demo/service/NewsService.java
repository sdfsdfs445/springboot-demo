package com.liyedong.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liyedong.demo.entity.News;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-07
 */
public interface NewsService extends IService<News> {

}
