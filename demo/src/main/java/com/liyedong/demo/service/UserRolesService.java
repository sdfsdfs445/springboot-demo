package com.liyedong.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liyedong.demo.entity.UserRoles;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
public interface UserRolesService extends IService<UserRoles> {

}
