package com.liyedong.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liyedong.demo.entity.Book;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 李烨栋
 * @since 2023-06-05
 */
public interface BookService extends IService<Book> {

}
