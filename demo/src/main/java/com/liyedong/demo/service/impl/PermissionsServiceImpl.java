package com.liyedong.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liyedong.demo.entity.Permissions;
import com.liyedong.demo.mapper.PermissionsMapper;
import com.liyedong.demo.service.PermissionsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
@Service
public class PermissionsServiceImpl extends ServiceImpl<PermissionsMapper, Permissions> implements PermissionsService {

}
