package com.liyedong.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liyedong.demo.entity.Roles;
import com.liyedong.demo.mapper.RolesMapper;
import com.liyedong.demo.service.RolesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
@Service
public class RolesServiceImpl extends ServiceImpl<RolesMapper, Roles> implements RolesService {

}
