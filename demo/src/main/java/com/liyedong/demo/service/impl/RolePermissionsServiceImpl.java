package com.liyedong.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liyedong.demo.entity.RolePermissions;
import com.liyedong.demo.mapper.RolePermissionsMapper;
import com.liyedong.demo.service.RolePermissionsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-03
 */
@Service
public class RolePermissionsServiceImpl extends ServiceImpl<RolePermissionsMapper, RolePermissions> implements RolePermissionsService {

}
