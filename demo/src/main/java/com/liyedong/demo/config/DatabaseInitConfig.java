package com.liyedong.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.annotation.PostConstruct;

@Configuration
public class DatabaseInitConfig {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseInitConfig.class);

    /**
     * jdbc_url
     */
    @Value("${spring.datasource.druid.url}")
    private String url;
    /**
     * 账号名称
     */
    @Value("${spring.datasource.druid.username}")
    private String username;
    /**
     * 账号密码
     */
    @Value("${spring.datasource.druid.password}")
    private String password;
    /**
     * 需要创建的数据名称
     */
    @Value("${CheckDatabase.DatabaseName}")
    private String DatabaseName;
    /**
     * 创建的数据库文件名
     */
    @Value("${CheckDatabase.DatabaseFilePath}")
    private String DatabaseFilePath;

    @PostConstruct
    public void init() {
        // 创建数据源
        DriverManagerDataSource dataSource = new DriverManagerDataSource(url.replace(DatabaseName,""),
                                            username,
                                            password);
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        if (!isDatabaseExists(jdbcTemplate,DatabaseName)){
            jdbcTemplate.execute("CREATE DATABASE IF NOT EXISTS " + DatabaseName);
            LOG.info("创建中控数据库（{}）成功", DatabaseName);
        }else {
            LOG.info("数据库（{}）存在", DatabaseName);
            return;
        }
        try{
            //修改路径，连接到数据库
            dataSource.setUrl(url);
            ClassPathResource rc = new ClassPathResource(DatabaseFilePath);
            EncodedResource er = new EncodedResource(rc, "utf-8");
            //通过spring工具类来执行sql脚本
            ScriptUtils.executeSqlScript(dataSource.getConnection(), er);
            LOG.info("执行数据库文件脚本（{}）成功", DatabaseFilePath);
        } catch (Exception e) {
            LOG.error("初始化系统数据库失败", e);
        }
    }
    public boolean isDatabaseExists(JdbcTemplate jdbcTemplate, String databaseName) {
        String sql = "SELECT COUNT(*) FROM information_schema.schemata WHERE schema_name = ?";
        int count = jdbcTemplate.queryForObject(sql, Integer.class, databaseName);
        return count > 0;
    }
}

