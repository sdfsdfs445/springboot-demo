package com.liyedong.demo.config;

import cn.dev33.satoken.stp.StpInterface;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.liyedong.demo.entity.Permissions;
import com.liyedong.demo.entity.RolePermissions;
import com.liyedong.demo.entity.Roles;
import com.liyedong.demo.entity.UserRoles;
import com.liyedong.demo.service.PermissionsService;
import com.liyedong.demo.service.RolePermissionsService;
import com.liyedong.demo.service.RolesService;
import com.liyedong.demo.service.UserRolesService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限加载接口实现类
 */
@Component    // 保证此类被 SpringBoot 扫描，完成 Sa-Token 的自定义权限验证扩展
public class StpInterfaceImpl implements StpInterface {

    @Resource
    PermissionsService permissionsService;
    @Resource
    RolesService rolesService;
    @Resource
    RolePermissionsService rolePermissionsService;
    @Resource
    UserRolesService userRolesService;
    /**
     * 返回一个账号所拥有的权限码集合 
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        //根据登录用户id查出用户的角色id
        LambdaQueryWrapper<UserRoles> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserRoles::getUserId,loginId);
        List<UserRoles> userRoles = userRolesService.list(queryWrapper);
        //根据角色id，查出角色的权限id
        LambdaQueryWrapper<RolePermissions> queryWrapper1=new LambdaQueryWrapper<>();
        for (int i=0;i<userRoles.size();i++){
            queryWrapper1.eq(RolePermissions::getRoleId,userRoles.get(i).getRoleId());
            if (i!=userRoles.size()-1){
                queryWrapper1.or();
            }
        }
        List<RolePermissions> rolePermissions = rolePermissionsService.list(queryWrapper1);
        //根据权限id，查出权限名
        LambdaQueryWrapper<Permissions> queryWrapper2=new LambdaQueryWrapper<>();
        for (int i=0;i<rolePermissions.size();i++){
            queryWrapper2.eq(Permissions::getPermissionId,rolePermissions.get(i).getPermissionId());
            if (i!=rolePermissions.size()-1){
                queryWrapper2.or();
            }
        }
        List<Permissions> permissions = permissionsService.list(queryWrapper2);
        List<String> list = new ArrayList<>();
        for (Permissions permission : permissions){
            list.add(permission.getPermissionName());
        }
        return list;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        //根据登录用户id查出用户的角色id
        LambdaQueryWrapper<UserRoles> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(UserRoles::getUserId,loginId);
        List<UserRoles> userRoles = userRolesService.list(queryWrapper);
        //根据角色id查出角色名
        LambdaQueryWrapper<Roles> queryWrapper1=new LambdaQueryWrapper<>();
        for (int i=0;i<userRoles.size();i++){
            queryWrapper1.eq(Roles::getRoleId,userRoles.get(i).getRoleId());
            if (i!=userRoles.size()-1){
                queryWrapper1.or();
            }
        }
        List<Roles> roles = rolesService.list(queryWrapper1);
        List<String> list = new ArrayList<>();
        for (Roles role : roles){
            list.add(role.getRoleName());
        }
        return list;
    }

}
