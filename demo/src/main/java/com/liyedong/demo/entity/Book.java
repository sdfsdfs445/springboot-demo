package com.liyedong.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-04
 */
@Getter
@Setter
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 作者
     */
    private String author;

    /**
     * 出版日期
     */
    private Date createTime;

    /**
     * 封面地址
     */
    private String cover;

    /**
     * 用户id
     */
    private Integer userId;
}
