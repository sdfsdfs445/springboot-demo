package com.liyedong.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-04
 */
@Getter
@Setter
public class Permissions implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "permission_id", type = IdType.AUTO)
    private Integer permissionId;
    private String permissionName;
}
