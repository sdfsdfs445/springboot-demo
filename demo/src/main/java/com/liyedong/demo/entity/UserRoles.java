package com.liyedong.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 李烨栋
 * @since 2024-03-04
 */
@Getter
@Setter
@TableName("demo.user_roles")
public class UserRoles implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "user_role_id", type = IdType.AUTO)
    private Integer userRoleId;
    private Integer userId;
    private Integer roleId;
}
